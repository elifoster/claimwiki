<?php
/**
 * Curse Inc.
 * Claim Wiki
 * Claim Wiki Aliases
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Claim Wiki
 * @link		https://gitlab.com/hydrawiki
 *
**/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'ClaimWiki' => ['ClaimWiki'],
	'WikiClaims' => ['WikiClaims']
];
